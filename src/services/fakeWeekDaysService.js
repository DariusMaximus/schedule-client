function getWeekDays() {
  return ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday"];
}

export default getWeekDays;
