import http from "./httpService";
import { API_END_POINT } from "../config/config";

export const getAppointments = async () => {
  return (await http.get(API_END_POINT)).data;
};

export const saveAppointment = (appointment, temporaryAppointment) => {
  if (temporaryAppointment.name) {
    return http.put(`${API_END_POINT}/${appointment.date}`, appointment);
  }

  return http.post(API_END_POINT, appointment);
};

export const deleteAppointment = (appointmentDate) => {
  return http.delete(`${API_END_POINT}/${appointmentDate}`);
};
