import Raven from "raven-js";

function init() {
  Raven.config(
    "https://138e20f699a441d7b395f11b6775b3fc@o507516.ingest.sentry.io/5608485",
    { release: "1-0-0", environment: "development-test" }
  ).install();
}

function log(error) {
  Raven.captureException(error);
}

// eslint-disable-next-line import/no-anonymous-default-export
export default {
  init,
  log,
};
