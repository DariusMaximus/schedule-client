import React from "react";
import ReactDOM from "react-dom";
import "bootstrap/dist/css/bootstrap.css";
import logger from "./services/logService.js";
import App from "./App";

logger.init();

ReactDOM.render(<App />, document.getElementById("root"));
