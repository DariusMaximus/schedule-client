import React, { Component } from "react";
import { toast } from "react-toastify";
import { ToastContainer } from "react-toastify";
import {
  deleteAppointment,
  getAppointments,
  saveAppointment,
} from "../../services/MongoDbService";
import getTimeslots from "../../services/fakeTimeslotsService";
import getWeekDays from "../../services/fakeWeekDaysService";
import BookingModal from "./bookingModal/BookingModal";
import TableHeader from "./tableHeader/tableHeader";
import logService from "../../services/logService";
import "react-toastify/dist/ReactToastify.css";
import TableBody from "./tableBody/tableBody";

class WeekTable extends Component {
  state = {
    weekDays: [],
    timeslots: [],
    appointments: [],
    temporaryAppointment: null,
    showModal: false,
  };

  async componentDidMount() {
    const { onError } = this.props;
    try {
      const weekDays = getWeekDays();
      const timeslots = getTimeslots();
      const appointments = await getAppointments();
      this.setState({ weekDays, timeslots, appointments });
    } catch (ex) {
      onError();
      logService.log(ex);
    }
  }

  handleShowModal = (name, date) => {
    const temporaryAppointment = { name, date };
    this.setState({
      showModal: true,
      temporaryAppointment,
    });
  };

  handleHideModal = () => {
    this.setState({ showModal: false });
  };

  getFilteredAppointments(arr, objectToFilter) {
    return arr.filter(
      (a) => a.date !== objectToFilter.date && a.name !== objectToFilter.name
    );
  }

  handleSaveAppointment = (e, name) => {
    e.preventDefault();
    const { appointments, temporaryAppointment } = this.state;
    const filteredAppointments = this.getFilteredAppointments(
      appointments,
      temporaryAppointment
    );
    let appointmentToSave = { date: temporaryAppointment.date, name: name };
    const updatedAppointments = [...filteredAppointments, appointmentToSave];
    this.setState({
      appointments: [...updatedAppointments],
      showModal: false,
    });
    saveAppointment(appointmentToSave, temporaryAppointment);
    toast.success("The appointment was scheduled successfully");
  };

  handleDeleteAppointment = (e, appointment) => {
    e.preventDefault();
    this.setState({ showModal: false });
    if (this.state.appointments.length) {
      const filteredAppointments = this.state.appointments.filter(
        (a) => a.date !== appointment.date
      );
      this.setState({ appointments: [...filteredAppointments] });
      deleteAppointment(appointment.date);
      toast("The appointment was deleted successfully");
    }
  };

  render() {
    const { weekDays, timeslots, appointments, showModal } = this.state;
    return (
      <>
        <ToastContainer />
        <h1 className="text-center m-5" style={{ color: "blue" }}>
          Welcome to the hair salon calendar !
        </h1>
        <table className="table table-bordered table-dark">
          <TableHeader weekDays={weekDays} />
          <TableBody
            weekDays={weekDays}
            timeslots={timeslots}
            timeslotsArr={appointments}
            onModal={this.handleShowModal}
          />
        </table>
        {showModal && (
          <BookingModal
            onSave={this.handleSaveAppointment}
            onClose={this.handleHideModal}
            onDelete={this.handleDeleteAppointment}
            temporaryAppointment={this.state.temporaryAppointment}
            name={this.state.temporaryAppointment.name}
          />
        )}
      </>
    );
  }
}

export default WeekTable;
