import TableData from "../tableBody/tableData/tableData.jsx";
import React from "react";

const getCustomerName = (arr, keyValue) => {
  const filteredArr = arr.find((obj) => obj.date === keyValue);

  return filteredArr ? filteredArr.name : "";
};

const TableBody = (props) => {
  const { timeslotsArr } = props;
  return (
    <tbody>
      {props.timeslots.map((timeslot, index) => (
        <tr key={index} className="text-center">
          <td key={index}>{timeslot}</td>
          {props.weekDays.map((weekDay, index) => (
            <TableData
              key={index}
              weekDay={weekDay}
              timeslot={timeslot}
              onModal={props.onModal}
              name={getCustomerName(timeslotsArr, `${weekDay} ${timeslot}`)}
            />
          ))}
        </tr>
      ))}
    </tbody>
  );
};

export default TableBody;
