import React from "react";

const TableData = (props) => {
  const { weekDay, timeslot, name } = props;
  const date = `${weekDay} ${timeslot}`;

  return (
    <td
      className="clickable"
      onClick={() => props.onModal(name, date)}
      style={{ cursor: "pointer" }}
    >
      {name}
    </td>
  );
};

export default TableData;
