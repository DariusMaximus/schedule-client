import React from "react";

const TableHeader = (props) => {
  return (
    <thead>
      <tr className="text-center">
        <th />
        {props.weekDays.map((day, index) => (
          <th key={index}>{day}</th>
        ))}
      </tr>
    </thead>
  );
};

export default TableHeader;
