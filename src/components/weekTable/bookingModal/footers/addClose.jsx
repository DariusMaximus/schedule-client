import React from "react";

const AddCloseFooter = (props) => {
  const { customerName, onClose, onSave } = props;
  return (
    <div key="modal footer">
      <button
        className="btn btn-danger"
        onClick={onClose}
        style={{ margin: "5px", width: "70px", height: "40px" }}
      >
        Close
      </button>
      <button
        className="btn btn-primary"
        onClick={(e) => onSave(e, customerName)}
        style={{ margin: "5px", width: "70px", height: "40px" }}
      >
        Add
      </button>
    </div>
  );
};

export default AddCloseFooter;
