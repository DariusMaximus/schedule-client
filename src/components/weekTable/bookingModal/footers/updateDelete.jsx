import React from "react";

const UpdateDeleteFooter = (props) => {
  const { onDelete, onSave, appointment, customerName } = props;
  return (
    <div key="modal footer">
      <button
        className="btn btn-danger"
        onClick={(e) => onDelete(e, appointment)}
        style={{ margin: "5px", width: "70px", height: "40px" }}
      >
        Delete
      </button>
      <button
        className="btn btn-primary"
        onClick={(e) => onSave(e, customerName)}
        style={{ margin: "5px", width: "70px", height: "40px" }}
      >
        Update
      </button>
    </div>
  );
};

export default UpdateDeleteFooter;
