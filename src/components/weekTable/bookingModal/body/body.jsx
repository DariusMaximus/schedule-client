import React from "react";

const ModalBookingBody = (props) => {
  const { customerName, onChange } = props;
  return (
    <input
      type="text"
      placeholder="Enter your name..."
      value={customerName}
      onChange={onChange}
    />
  );
};

export default ModalBookingBody;
