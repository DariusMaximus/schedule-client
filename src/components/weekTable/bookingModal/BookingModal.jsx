import React, { Component } from "react";
import UpdateDeleteFooter from "./footers/updateDelete.jsx";
import AddCloseFooter from "./footers/addClose.jsx";
import ModalBookingBody from "./body/body.jsx";
import "./BookingModal.css";

class BookingModal extends Component {
  state = {
    customerName: this.props.name || "",
  };

  handleOnChange = (e) => {
    this.setState({ customerName: e.target.value });
  };

  render() {
    const { temporaryAppointment, onSave, onDelete, onClose } = this.props;

    return (
      <div key="Booking Modal" className={"modal"}>
        <div className="modalContainer">
          <button className="modalCloseButton" onClick={onClose}>
            X
          </button>
          <form className="modalForm">
            <h3>Book a haircut</h3>
            <ModalBookingBody
              customerName={this.state.customerName}
              onChange={this.handleOnChange}
            />
            {temporaryAppointment.name ? (
              <UpdateDeleteFooter
                customerName={this.state.customerName}
                appointment={temporaryAppointment}
                onDelete={onDelete}
                onClose={onClose}
                onSave={onSave}
              />
            ) : (
              <AddCloseFooter
                customerName={this.state.customerName}
                onClose={onClose}
                onSave={onSave}
              />
            )}
          </form>
        </div>
      </div>
    );
  }
}

export default BookingModal;
