import React from "react";
import errorImage from "./pexels-photo-SWW.jpeg";
import "./errorPage.css";

const ErrorPage = () => {
  // eslint-disable-next-line jsx-a11y/img-redundant-alt
  return <img src={errorImage} alt="This is a 404 error image" />;
};

export default ErrorPage;
