import "./App.css";
import React, { Component } from "react";
import ErrorPage from "./components/errorPage/ErrorPage";
import WeekTable from "./components/weekTable/WeekTable";

class App extends Component {
  state = {
    showError: false,
  };

  handleErrorPage() {
    this.setState({ showError: true });
  }

  render() {
    const { showError } = this.state;
    return (
      <main className="container">
        {showError ? (
          <ErrorPage />
        ) : (
          <WeekTable onError={this.handleErrorPage.bind(this)} />
        )}
      </main>
    );
  }
}

export default App;
